import {useReducer} from "react";
import {makeDayKeys} from "./utils";

/**
 * @typedef {Object} UpdatePayload
 * @property {string} dayKey
 * @property {ticket} ticketId
 * @property {number} amount
 */

/**
 * @typedef {Object} TimeTrackState
 * @property {Ticket[]} tickets
 * @property {Map<string, Map<string, number>>} ticketHours
 *
 */

/**
 *
 * @param {TimeTrackState} state
 * @param {object} action
 * @param {object} action.payload
 * @param {string} action.type
 * @returns {*}
 */
function timeTrackReducer(state, action) {
    const newState = {...state};
    switch (action.type) {
        case "update": {
            /** type */
            const payload = action.payload;
            newState[payload.taskId][payload.dayKey] = payload.amount;
            break;
        }
        default:
            throw new Error("Unexpected action: " + action.type);
    }
    window.localStorage.setItem("timeTrackState", JSON.stringify(newState));
    return newState;
}

function createInitialState(tickets) {
    return tickets.reduce((acc, ticket) => {
        acc[ticket.taskId] = {};
        return acc;
    }, {});
}

function getInitialState(tickets) {
    const storage = JSON.parse(window.localStorage.getItem("timeTrackState"));
    return storage ? storage : createInitialState(tickets);
}

export default function useTimeTrack(tickets) {
    const initialState = getInitialState(tickets);
    const [state, dispatch] = useReducer(timeTrackReducer, initialState);

    function updateTimeTrack(taskId, dayKey, amount) {
        dispatch({type: "update", payload: {dayKey, taskId, amount}});
    }

    /**
     *
     * @param taskId
     * @returns {*}
     */
    function getTicketAmounts(taskId) {
        return state[taskId];
    }

    function getTotalAmounts(dayKey) {
        let amount = 0;
        const ticketIds = Object.keys(state);
        for (const ticketId of ticketIds) {
            const ticket = state[ticketId];
            amount += ticket[dayKey] ? Number.parseFloat(ticket[dayKey]) : 0;
        }
        return amount;
    }

    /**
     *
     * @param taskId
     * @param year
     * @param month
     * @returns {number[]}
     */
    function getHours(taskId, year, month) {
        const amounts = getTicketAmounts(taskId);
        /** @type {string[]}*/
        const dayKeys = makeDayKeys(year, month);
        console.log({dayKeys});
        const monthAmounts = [];
        dayKeys.forEach((dayKey) => {
                const value = amounts[dayKey] ? amounts[dayKey] : 0;
                console.log(monthAmounts);
                return monthAmounts.push(value);
            }
        );
        console.log(monthAmounts);
        return monthAmounts;
    }

    return {updateTimeTrack, getTicketAmounts, getTotalAmounts, getHours};
}
