import { makeDayKeys } from "./utils";

describe("it can generate a month", () => {
  it("... given february 2021 to return 28", () => {
    const actual = makeDayKeys(2021, 1);
    expect(actual).toHaveLength(28);
    for (let i = 0; i < 28; i++) {
      expect(actual[i]).toBe("2021-1-" + (i + 1));
    }
  });
  it("... given May 2021 to return 31", () => {
    const actual = makeDayKeys(2021, 4);
    expect(actual).toHaveLength(31);
    for (let i = 1; i < 31; i++) {
      expect(actual[i]).toBe("2021-4-" + (i + 1));
    }
  });
  it("... given february 2024 to return 29", () => {
    const actual = makeDayKeys(2024, 1);
    expect(actual).toHaveLength(29);
    for (let i = 1; i < 29; i++) {
      expect(actual[i]).toBe("2024-1-" + (i + 1));
    }
  });
});
