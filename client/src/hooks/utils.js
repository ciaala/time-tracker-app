export function fromDayToDayKey(day) {
  return `${day.getFullYear()}-${day.getMonth()}-${day.getDate()}`;
}

/**
 *
 * @param year
 * @param month
 * @returns {string[]}
 */
export function makeDayKeys(year, month) {
  console.log(`${year}-${month}`);
  const result = [];
  for (let i = 1; i < 32; i++) {
    const day = new Date(year, month, i);
    if (day.getMonth() === month) {
      const dayKey = fromDayToDayKey(day);
      result.push(dayKey);
    } else {
      break;
    }
  }
  return result;
}
