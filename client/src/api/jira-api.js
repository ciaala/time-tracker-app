async function call(started, timeSpent, ticket, commentText) {
  const customComment = {
    version: 1,
    type: "doc",
    content: [
      {
        type: "paragraph",
        content: [
          {
            type: "text",
            text: "daily",
          },
        ],
      },
    ],
  };
  const emptyComment = {
    type: "doc",
    version: 1,
    content: [],
  };
  const comment = commentText ? customComment : emptyComment;
  const payload = {
    comment,
    started,
    timeSpent,
    ticket,
  };
 // const requestUrl = `https://appway.atlassian.net/rest/internal/3/issue/${ticket}/worklog`;
  const requestUrl = '/jira/worklog';
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      // "Access-Control-Allow-Origin": "*",
      // "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, HEAD, OPTIONS",
    },
    mode: "cors",
    body: JSON.stringify(payload),
  };

  const response = await fetch(requestUrl, options);
  console.log(response);
  return response.json();
}

async function generateTicket(
  ticket,
  year,
  month,
  hourOfTicket,
  minuteOfTicket,
  amounts,
  realCall
) {
  const daysInMonth = new Date(year, month + 1, 0).getDate();
  for (let day = 1; day <= daysInMonth; day++) {
    if (amounts.length < day || amounts[day - 1] === 0) {
      continue;
    }
    const date = new Date(year, month, day, hourOfTicket, minuteOfTicket);

    if (!(date.getDay() === 0 || date.getDay() === 6)) {
      const asString = date.toISOString();
      const hours = -date.getTimezoneOffset() / 60;
      const withoutZ = asString.substr(0, asString.length - 1) + `+0${hours}00`;
      if (realCall) {
        console.log("Calling " + ticket + " " + withoutZ);
        await call(withoutZ, amounts[day - 1].toString() + "h", ticket);
      } else {
        console.log(withoutZ, amounts[day - 1].toString() + "h", ticket);
      }
    }
  }
}

/**
 *
 * @param {Ticket[]} tickets
 * @param {number} year
 * @param {number} month
 * @param {Object<string, number[]>} hours
 * @param {boolean} realCall
 */
export default async function generateWorkLogs(
  tickets,
  year,
  month,
  hours,
  realCall
) {
  // console.log(tickets);
  // const asyncCalls = [];
  for (let i = 0; i < tickets.length; i++) {
    //const asyncCall = new Promise(() => {
    const ticket = tickets[i];
    console.log(ticket.title);
    await generateTicket(
      ticket.taskId,
      year,
      month,
      ticket.defaultHour,
      ticket.defaultMinute,
      hours[ticket.taskId],
      realCall
    );

    // });
    // asyncCalls.push(asyncCall);
  }
  // return Promise.all(asyncCalls);
}
