export function getInfo() {
    return fetch('/server/version').then(data => data.json());
}