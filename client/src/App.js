// import logo from './logo.svg';
import "./App.module.css";
import MonthViewer from "./components/MonthViewer/MonthViewer";
import TicketsBar from "./components/TicketBar/TicketsBar";
import useTimeTrack from "./hooks/useTimeTrack";
import {useState} from "react";
import SelectMonth from "./components/SelectMonth/SelectMonth";
import Button from "./components/Button/Button";
import generateWorkLogs from "./api/jira-api";
import Footer from "./components/layout/Footer/Footer";
import ServerVersion from "./ServerVersion";


/**
 *
 * @returns {Ticket}
 */
function newTicket(
    title,
    taskId,
    parentEpicId,
    salesforceOrder,
    defaultHour,
    defaultMinute = 0
) {
    return {
        title,
        taskId,
        defaultHour,
        parentEpicId,
        salesforceOrder,
        defaultMinute,
    };
}

/**
 *
 * @type {Ticket[]}
 */
const tickets = [
    newTicket("DeploymentProfile", "AP-40174", "AP-40174", "11.0", 14, 0),
    newTicket("Import / Export", "AP-33875", "AP-33875", "11.0", 10, 0),
    newTicket("Old shit Unplanned", "AP-39488", "AP-39488", "10.1", 14, 0),
    newTicket(
        "Team meetings (Dailies, Retro, Refinement, Planning, Self-Organization)",
        "AP-39549",
        "AP-39549",
        "11.0",
        9,
        30
    ),
];

export default function App() {
    const [currentMonthIndex, setCurrentMonthIndex] = useState(
        new Date().getMonth()
    );
    const [currentTicketIndex, setCurrentTicketIndex] = useState(0);
    const {updateTimeTrack, getTicketAmounts, getTotalAmounts, getHours} =
        useTimeTrack(tickets);

    function update(dayKey, amount) {
        console.log(dayKey, amount);
        const taskId = tickets[currentTicketIndex].taskId;
        updateTimeTrack(taskId, dayKey, Number.parseFloat(amount));
    }

    // let ticketAmounts = getTicketAmounts(taskId);
    // useEffect(() => {
    //   const taskId = tickets[currentTicketIndex].taskId;
    //   console.log("calculated");
    //   ticketAmounts = getTicketAmounts(taskId);
    // }, [currentTicketIndex]);

    function getAmounts(dayKey) {
        const taskId = tickets[currentTicketIndex].taskId;
        const ticketAmounts = getTicketAmounts(taskId);
        //console.log("getAmounts " + dayKey);
        return ticketAmounts[dayKey];
    }

    function sendTickets() {
        const onlyTickets = [...tickets];
        console.log(onlyTickets);
        const hours = {};
        for (let index of Object.keys(onlyTickets)) {
            const ticket = onlyTickets[index];
            hours[ticket.taskId] = getHours(ticket.taskId, 2021, currentMonthIndex);
        }
        console.log({tickets, hours});
        generateWorkLogs(onlyTickets, 2021, currentMonthIndex, hours, true).then(
            (data) => alert("Job Done: " + data)
        );
    }

    const day = new Date(2021, currentMonthIndex, 0);

    return (
        <>
            <SelectMonth
                currentMonthIndex={currentMonthIndex}
                handleOnChange={setCurrentMonthIndex}
            />
            <TicketsBar
                tickets={tickets}
                currentTicketIndex={currentTicketIndex}
                setCurrentTicketIndex={(value) => {
                    setCurrentTicketIndex(value);
                }}
            />
            <MonthViewer
                month={day.getMonth()}
                year={day.getFullYear()}
                update={update}
                getAmounts={getAmounts}
            />
            <MonthViewer
                month={day.getMonth()}
                year={day.getFullYear()}
                update={update}
                getAmounts={getTotalAmounts}
            />

            <Button label={"send"} onClick={sendTickets}/>
            <Footer><ServerVersion/></Footer>
        </>

    );
}
