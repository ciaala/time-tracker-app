import {useEffect, useRef, useState} from "react";
import {getInfo} from "./api/server-api";

function Connecting() {

    return <span>Connecting ...</span>;
}

export default function ServerVersion() {
    const [versionInfo, setVersionInfo] = useState();
    const mounted = useRef(true);

    useEffect(() => {
            console.log('useEffect p1');
            mounted.current = true;
            if (versionInfo) {
                return;
            }
            console.log('useEffect p2');
            getInfo()
                .then((data) => {
                    if ( mounted.current ) {
                        console.log('useEffect p3');
                        setVersionInfo(data);
                    }
            });
            return () => mounted.current = false;
        }, []
    );
    const versionValue = versionInfo ? <span>{versionInfo["Node Version"] + ' on ' + versionInfo["Express Version"]}</span> : <Connecting/>;
    return <span>{versionValue}</span>;
}