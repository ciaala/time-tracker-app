//import styles from "./TicketDropDown.module.css";

/**
 *
 * @param {Ticket[]} props.tickets
 * @param {function} props.setCurrentTicketIndex
 * @returns {JSX.Element}
 * @constructor
 */
export function TicketDropDown(props) {
  const children = [];

  function handleOnSelect(event) {
    props.setCurrentTicketIndex(event.target.value);
  }

  for (let index = 0; index < props.tickets.length; index++) {
    const ticket = props.tickets[index];
    children.push(
      <option key={index} value={index}>
        {ticket.title}
      </option>
    );
  }
  return <select onChange={handleOnSelect}>{children}</select>;
}
