export function TicketOneLiner({ ticket }) {
  return (
    <>
      <span>{ticket.title}</span>
      <span>-</span>
      <span>{ticket.parentEpicId}</span>
      <span>{ticket.taskId}</span>
      <span>{ticket.salesforceOrder}</span>
    </>
  );
}
