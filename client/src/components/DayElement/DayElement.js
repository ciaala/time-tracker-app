import styles from "./DayElement.module.css";

function getDayOfWeek(date) {
  const DAYS = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  return DAYS[date.getDay()];
}

function concatClassName(...classes) {
  return classes.join(" ");
}
export default function DayElement({ day, dayKey, getAmount, handleOnChange }) {
  // console.log(dayKey);
  //   const todayClassName =
  //     today.getDate() === day.getDate() ? styles.today : undefined;

  const workDayClassName =
    day.getDay() !== 0 && day.getDay() !== 6
      ? styles.workDay
      : styles.vacationDay;

  return (
    <div
      className={concatClassName(workDayClassName, styles.dayElement)}
      key={dayKey}
    >
      <span className={styles.number}>{day.getDate()}</span>
      <span className={styles.dayOfTheWeek}>{getDayOfWeek(day)}</span>
      <input
        className={styles.amount}
        maxLength={4}
        type="number"
        value={getAmount(dayKey)}
        max={24}
        min={0}
        step={0.5}
        onChange={(event) => handleOnChange(dayKey, event)}
      />
    </div>
  );
}
