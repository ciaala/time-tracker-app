import styles from "./Button.module.css";
export default function Button(props) {
  return (
    <input
      className={styles.actionButton}
      type="button"
      value={props.label}
      onClick={props.onClick}
    />
  );
}
