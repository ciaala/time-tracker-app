import styles from "./MonthViewer.module.css";
import DayElement from "../DayElement/DayElement";
import { fromDayToDayKey } from "../../hooks/utils";

function WeakSumElement(props) {
  return (
    <div className={styles.weakElement} >
      <span>{props.amount}</span>
    </div>
  );
}

/**
 *
 * @param {number} props.year
 * @param {number} props.month
 * @param {Object<string,number>} props.values
 * @param {function} props.update
 * @param {function} props.getAmounts
 * @returns {JSX.Element}
 * @constructor
 */
export default function MonthViewer(props) {
  const month = props.month + 1;

  // const [values, setValues] = useState(props.values ? props.values : {});
  function handleOnChange(dayKey, event) {
    props.update(dayKey, event.target.value);
  }

  function getAmount(dayKey) {
    const value = props.getAmounts(dayKey);
    return value ? value : 0;
  }

  //const today = new Date();
  //console.log(today);

  let firstDay = new Date(props.year, month + 1, 0);
  const daysInMonth = firstDay.getDate() + 1;

  const dayElements = [];
  const days = [];
  const firstWeekDay = new Date(props.year, month, 1).getDay();
  const times = (-1 + firstWeekDay) % 7;
  for (let i = 0; i < times; i++) {
    dayElements.push(<div key={"empty" + i} />);
  }
  for (let i = 1; i < daysInMonth; i++) {
    const day = new Date(props.year, month, i);
    days.push(day);
    const dayKey = fromDayToDayKey(day);

    const element = (
      <DayElement
        key={dayKey}
        dayKey={dayKey}
        day={day}
        getAmount={getAmount}
        handleOnChange={handleOnChange}
      />
    );

    dayElements.push(element);

    const isLastDay = day.getDay() === 0;
    if (isLastDay) {
      let amount = 0;
      let start = days.length > 7 ? days.length - 7 : 0;
      for (let i = start; i < days.length; i++) {
        const weekDay = days[i];
        amount += getAmount(fromDayToDayKey(weekDay));
      }
      dayElements.push(
        <WeakSumElement key={"week-" + dayKey} amount={amount} />
      );
    }
  }

  return (
    <div>
      <span>{days[0].toLocaleDateString()}</span>
      <div className={styles.main}>{dayElements}</div>
    </div>
  );
}
