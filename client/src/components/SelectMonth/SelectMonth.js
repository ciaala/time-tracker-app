export default function SelectMonth(props) {
  function handleOnChange(event) {
    props.handleOnChange(Number(event.target.value));
  }
  const monthElements = [];
  // const months = [];
  const date = new Date();
  for (let i = 0; i < 12; i++) {
    date.setMonth(i);
    const month = date.toLocaleDateString("en", { month: "long" });
    //months.push(month);
    const monthElement = <option key={i} value={i} label={month} />;
    monthElements.push(monthElement);
  }
  return <select onChange={handleOnChange} value={props.currentMonthIndex}>{monthElements}</select>;
}
