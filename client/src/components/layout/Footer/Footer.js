import styles from './Footer.module.css';

export default function Footer(props) {

    return <div className={styles.main}>{props.children}</div>;
}