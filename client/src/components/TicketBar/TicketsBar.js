import { TicketOneLiner } from "../TicketOneLiner/TicketOneLiner";
import { TicketDropDown } from "../TicketDropDown/TicketDropDown";

/**
 *
 * @param {Ticket[]} props.tickets
 * @param {number} props.currentIndex
 * @param {function} props.setCurrentTicketIndex
 * @returns {null}
 * @constructor
 */
export default function TicketsBar(props) {
  const ticket = props.tickets[props.currentTicketIndex];
  return (
    <div>
      <TicketOneLiner ticket={ticket} />
      <TicketDropDown
        tickets={props.tickets}
        currentTicketIndex={props.currentTicketIndex}
        setCurrentTicketIndex={props.setCurrentTicketIndex}
      />
    </div>
  );
}
