/**
 * @typedef {Object} Ticket
 * @property {string} title
 * @property {string} taskId
 * @property {string} parentEpicId
 * @property {string} salesforceOrder
 * @property {number} defaultHour
 * @property {number} defaultMinute
 */
