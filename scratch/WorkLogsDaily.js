{

    async function call(started, timeSpent, ticket, commentText) {
        const customComment = {
            "version": 1,
            "type": "doc",
            "content": [{
                "type": "paragraph",
                "content": [{
                    "type": "text",
                    "text": "daily"
                }]
            }]
        };
        const emptyComment = {
            type: "doc",
            version: 1,
            content: [],
        };
        const comment = commentText ? customComment : emptyComment;
        const payload = {
            comment,
            started,
            timeSpent
        };
        const requestUrl = `https://appway.atlassian.net/rest/internal/3/issue/${ticket}/worklog`
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            mode: 'cors',
            body: JSON.stringify(payload)
        }
        const response = await fetch(requestUrl, options);
        console.log(response);
        return response.json();

    }

    function generateTicket(ticket, year, month, hourOfTicket, minuteOfTicket, amounts, realCall) {
        const daysInMonth = new Date(year,month + 1,0).getDate();
        for (let day = 1; day <= daysInMonth; day++) {
            if (amounts.length < day || amounts[day - 1] === 0) {
                continue;
            }
            const date = new Date(year,month,day,hourOfTicket,minuteOfTicket);

            if (!(date.getDay() == 0 || date.getDay() == 6)) {
                const asString = date.toISOString();
                const hours = -date.getTimezoneOffset() / 60;
                const withoutZ = asString.substr(0, asString.length - 1) + `+0${hours}00`;
                if (realCall) {
                    call(withoutZ, amounts[day - 1].toString() + "h", ticket);
                } else {
                    console.log(withoutZ, amounts[day - 1].toString() + "h", ticket);
                }
            }
        }
    }
    const FRIDAY = 5;
    const THURSDAY = 4;
    const MONDAY = 1;
    const WEDNESDAY = 3;
    const TUESDAY = 2;

    let WORK_WEEK = [FRIDAY, THURSDAY, MONDAY, WEDNESDAY, TUESDAY];

    function automaticTimeTracking(year, month, daysOfWeek, amount) {
        const daysInMonth = new Date(year,month + 1,0).getDate();
        const schedule = [];
        for (let day = 1; day <= daysInMonth; day++) {
            const date = new Date(year,month,day);

            if (daysOfWeek.indexOf(date.getDay()) !== -1) {
                schedule.push(amount)
            } else {
                schedule.push(0);
            }
            console.log(date, schedule[schedule.length - 1]);
        }

        return schedule;
    }

    //const ap40174 = [0,0,0,0,0,0,0, 0,0,0,0,0,0,0, 3,3,];
    //const realCall = false;
    //generateMonth('AP-33875', 2021, 2, 10, ap33875, realCall);
    //generateMonth('AP-40174', 2021, 4, 9, 30, hoursAP40174, realCall);
    // automaticTimeTracking(2021, 3, WORK_WEEK, 0.5)
    // automaticTimeTracking(2021, 3, [THURSDAY], 2);

    function monthSchedule(tickets, year, month) {
        const daysInMonth = new Date(year,month + 1,0).getDate();
        const schedule = [];
        for (let day = 1; day <= daysInMonth; day++) {
            let amount = 0;
            for (let i = 0; i < tickets.length; i++) {
                const ticket = tickets[i];
                amount += ticket.hours[day - 1];
            }
            schedule.push(amount)
        }
        return schedule;
    }
    function viewAsWeek(hours, year, month) {
        const daysInMonth = new Date(year,month + 1,0).getDate();
        const weeks = [];
        let week = [];
        weeks.push(week);
        for (let day = 1; day <= daysInMonth; day++) {
            let date = new Date(year,month,day);

            if (date.getDay() <= FRIDAY && date.getDay() >= MONDAY) {
                week.push(hours[day - 1]);
            }
            if (date.getDay() < MONDAY) {
                week = [];
                weeks.push(week)
            }
        }
        for (let i = 0; i < weeks.length; i++) {
            const week = weeks[i];
            const sum = week.reduce((acc,value)=>acc += value, 0);
            console.log(sum + ' == ' + week.toString());
        }
    }

    function generateTickets(tickets, year, month, realCall) {
        for (let i = 0; i < tickets.length; i++) {
            const ticket = tickets[i];
            console.log(ticket.title);
            generateTicket(ticket.ticket, year, month, ticket.defaultHour, ticket.defaultMinute, ticket.hours, realCall);
        }
    }

    const DeploymentProfile = {
        title: 'DeploymentProfile',
        ticket: 'AP-40174',
        defaultHour: 14,
        defaultMinute: 0,
        hours: [0, 0, 0, 0,//
            0, 0, 0, 0, 0, 0, 0, //
            0, 2, 2, 3, 2, 0, 0,//
            3, 3, 2, 0, 0, 0, 0,//
            0, 2, 2.5, 2.5, 5]
    };
    const ImportExport = {
        title: 'Import / Export',
        ticket: 'AP-33875',
        defaultHour: 10,
        defaultMinute: 0,
        hours: [4.5, 0, 0, 0, //
            0, 4, 5.5, 2, 3.5, 0, 0,
            5.5, 5.5, 5.5, 4.5, 2, 0, 0, //
            4.5, 4.5, 5.5, 3.5, 6, 0, 0,//
            4, 2, 5, 5, 0]
    };

    const ticket101 = {
        title: 'Old shit Unplanned',
        ticket: 'AP-39488',
        defaultHour: 14,
        defaultMinute: 0,
        hours: [6, 0, 0, 0, //
            0, 4, 2, 0, 0, 0, 0,//
            0, 2, 2, 0, 2, 0, 0,//
            2, 2, 0, 2, 0, 0, 0,//
            2, 2, 0, 2, 2],
    }
    const Daily = {
        title: 'Team meetings (Dailies, Retro, Refinement, Planning, Self-Organization)',
        ticket: 'AP-39549',
        defaultHour: 9,
        defaultMinute: 30,
        hours: [0.5, 0, 0, 0, //
            0,     2, 0.5, 2, 0.5, 0, 0,//
            0.5, 0.5, 0.5, 1.5, 0.5, 0, 0,//
            0.5, 0.5, 0.5, 2.5, 0.5, 0, 0,//
            2, 2, 0.5, 1.5, 0.5],
    };
    const Sprint = {
        title: 'Sprint',
        ticket: 'AP-39482',
        defaultHour: 9,
        defaultMinute: 0,
        hours: [0, 0, 0, 0, //
            0, 0, 0, 2, 0, 0, 0, //
            0, 0, 0, 0, 0, 0, 0, //
            0, 0, 0, 2, 0, 0, 0, //
            0, 0, 0, 0, 0],
    }
    const TimeTracking = {
        title: 'Time Tracking',
        ticket: 'AP-39596',
        defaultHour: 17,
        defaultMinute: 0,
        hours: [0, 0, 0, 0, //
            0, 0, 0, 0, 0, 0, 0, //
            0, 0, 0, 0, 1, 0, 0, //
            0, 0, 0, 0, 1, 0, 0, //
            0, 0, 0, 0, 1],
    };
    const Salesforce = {
        title: 'Salesforce',
        ticket: 'AP-40601',
        defaultHour: 14,
        defaultMinute: 0,
        hours: [0, 0, 0, 0, //
            0, 0, 0, 0, 0, 0, 0, //
            3, 0, 0, 0, 1, 0, 0, //
            0, 0, 0, 0, 1, 0, 0, //
            0, 2, 0, 0, 1],
    };
    const OneOnOne = {
        title: '1 - 1',
        ticket: 'AP-39483',
        defaultHour: 12,
        defaultMinute: 0,
        hours: [1, 0, 0, 0, //
            0, 0, 0, 0, 0, 0, 0, //
            0, 0, 0, 0, 0.5, 0, 0, //
            0, 0, 0, 0, 0.5, 0, 0, //
            0, 0, 0, 0, 0.5],
    };

    let tickets = [ImportExport, DeploymentProfile, ticket101];
    let process = [Daily, OneOnOne, TimeTracking];

    let allTickets = [ImportExport, DeploymentProfile, ticket101, Salesforce,
        Sprint, Daily, OneOnOne, TimeTracking]

    const schedule = generateMonth(allTickets, 2021, 3);
    viewAsWeek(schedule, 2021, 3);
    generateTickets(allTickets, 2021, 3, true);
}
