== Resources


=== Styling for React: CSS and Components

- https://blog.bitsrc.io/5-ways-to-style-react-components-in-2019-30f1ccc2b5b[Styling components]
- https://create-react-app.dev/docs/adding-a-css-modules-stylesheet[CSS Module]
- https://css-tricks.com/snippets/css/complete-guide-grid/[CSS Grid]
- https://codeburst.io/a-complete-guide-to-css-modules-in-react-part-2-b7bad1f44463[Css Modules and React]