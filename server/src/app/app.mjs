//const express = require('express')
import express from 'express';
import register from "./api/jira.mjs";
import bodyParser from "body-parser";

const app = express();
app.get('/', (request, response) => {
    response.send('Hello World');
});
app.use(bodyParser.json())
register(app);

export default app;
