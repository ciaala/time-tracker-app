import fetch from 'node-fetch';
import encoded from "../../security/token.mjs";
/**
 *
 * @param {Request} request
 * @param {Response} response
 */
function handle_jira_worklog(request, response) {
    const r = {'url': request.url, 'method': request.method, "payload": request.body};
    console.log(`${r.method} ${r.url} ${r.payload}`);
    const {ticket, comment, timeSpent, started} = r.payload;
    console.log({ticket, comment, timeSpent, started});
    // const requestUrl = `https://appway.atlassian.net/rest/internal/3/issue/${ticket}/worklog`
    const requestUrl = `https://appway.atlassian.net/rest/api/latest/issue/${ticket}/worklog`;
    const payload = {comment, started, timeSpent};

    const options = {
        method: 'POST',
        headers: {
            'Authorization' : 'Basic ' + encoded(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        mode: 'cors',
        body: JSON.stringify(payload)
    }
    send(requestUrl, options).then((data) => {
        console.log({data});
        response.send(data);
    });
  //  response.sendStatus(200);
}

async function send(requestUrl, options) {
     const response = await fetch(requestUrl, options);
     console.log(response);
     const json = await response.json();
     console.log(json);
     return json;
}

export default function register(app) {

    app.post('/jira/worklog', handle_jira_worklog);
    // app.get('/jira/worklog', handle_jira_worklog);
}