/**
 * @typedef {Object} Properties
 * @property {string} jira.username
 * @property {string} jira.apiToken
 */
import properties from "./secret.mjs";

export default function encoded() {
    const jira = properties.jira;
    const value = `${jira.username}:${jira.apiToken}`;
    const buffer = Buffer.from(value, 'utf8');
    const encoded = buffer.toString('base64');
    return encoded;
}
