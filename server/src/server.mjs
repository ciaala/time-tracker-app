import http from 'http';
import app from './app/app.mjs';

import {readFileSync} from 'fs';


const filename = 'package.json';
const pjson = JSON.parse(readFileSync(filename));

const port = process.env.PORT || 5000;

const info = {
    "version" :  {
        "Node Version": process.version,
        "Express Version": pjson.dependencies.express,
        "Port" : port
    }
}


app.get('/server/version',
    (request, response) => {
        console.log(request);
        response.send(info.version);
    }
);
const server = http.Server(app);

server.listen(port, () => {
    console.log("Request");
    return true;
});